const express = require("express");

let app = express();
app.use(express.static('./'));
app.use(express.static('views'));
app.use(express.static('static'));


app.get('/',         function(req, res){ res.sendFile("views/index.html"   , { root: __dirname }); });
app.get('/photos',   function(req, res){ res.sendFile("views/photos.html"  , { root: __dirname }); });
app.get('/projects', function(req, res){ res.sendFile("views/projects.html", { root: __dirname }); });


app.listen(8080, () => {
    console.log("Server listening!");
})